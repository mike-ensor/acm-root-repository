#!/bin/bash -x
unset K8S_COUNT

CLUSTER_NAME_PREFIX="k3s"
DRY_RUN="false"

# maybe not hardcode in the future
CUSTOMERS=("customer-1" "customer-2" "customer-3")

while getopts c:d option
do
    case "${option}" in
        c) K8S_COUNT=${OPTARG};;
        d) DRY_RUN="true"
    esac
done

if [[ -z "$K8S_COUNT" ]]; then
    echo "Missing count variable"
    echo "Usage: ./create-cluster-selectors.sh -c <INT>"
    exit 0
fi

i=1
while [[ $i -le ${K8S_COUNT} ]]; do
    echo "Setting up Cluster object '$i'"
    CLUSTER_NAME="${CLUSTER_NAME_PREFIX}-$c"
    n=$(($c%3))
    CUST="${CUSTOMERS[$n]}"

    if [ ${DRY_RUN} == "false" ]; then
        sed -e "s|%%CLUSTER_NAME%%|${CLUSTER_NAME}|g" -e "s|%%CUSTOMER%%|${CUST}|g" cluster-template.yaml.tpl
        sed -e "s|%%CLUSTER_NAME%%|${CLUSTER_NAME}|g" -e "s|%%CUSTOMER%%|${CUST}|g" cluster-selector-template.yaml.tpl
    else
        sed -e "s|%%CLUSTER_NAME%%|${CLUSTER_NAME}|g" -e "s|%%CUSTOMER%%|${CUST}|g" cluster-template.yaml.tpl > "config/clusterregistry/$CLUSTER_NAME-cluster.yaml"
        sed -e "s|%%CLUSTER_NAME%%|${CLUSTER_NAME}|g" -e "s|%%CUSTOMER%%|${CUST}|g" cluster-selector-template.yaml.tpl > "config/clusterregistry/$CLUSTER_NAME-cluster-selector.yaml"
        # git status
        # git add . && git commit -m 'adding ClusterSelectors' && git push
    fi

    let i=i+1
done


