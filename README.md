# Overview

This is an example working git repository for Anthos Config Management enabled for Multi-Repo configuration

## Obtaining Cluster Viewer Token (login to cluster)

```bash
# 'console-cluster-reader' is a KSA defined in /config/namespaces/default/console-cluster-reader-sa.yaml
SECRET_NAME=$(kubectl get serviceaccount console-cluster-reader -o jsonpath='{$.secrets[0].name}')

TOKEN=$(kubectl get secret ${SECRET_NAME} -o jsonpath='{$.data.token}' | base64 --decode)

echo -e "\n\n${TOKEN}\n\n"

```

# Adding selector

```yaml
...
...
metadata:
  name: some-krm-name
  annotations:
    configmanagement.gke.io/cluster-selector: name-of-selector
...
...
```